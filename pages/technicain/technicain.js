//require获取本机其他js文件并保存到变量
var util = require("../../utils/util.js")
var data = require("../../utils/data.js")
// pages/technicain/technicain.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 美容项目数组
    casArray:["美发","美容","美甲","美睫"],
    casIndex:0,
    addrPicker: util.replacePhone(data.userData().addrs,false),
    addrIndex:0,
    technicianData:data.getSkilledData(),
    curNavID:1,
    curIndex:0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // this.setData({
    //   list: this.data.technicianData
    // })
    // 获取所有技师信息/techn/queryAll
    wx.request({
      url: 'http://localhost:8080/techn/queryAll',
      success:function(res){
        console.log(res)
        that.setData({
          list: res.data.data
        })
      }
    })
  },
  // 同步美容项目选择内容
  bindCasPickerChange:function(event){
    this.setData({
      casIndex:event.detail.value
    })
  },
  // 同步地址选择内容
  bindAddrPickerChange: function (event) {
    this.setData({
      addrIndex: event.detail.value
    })
  },
  // 跳转技师详情
  navigateDetail:function(event){
    var tId = event.currentTarget.dataset.id
    wx.navigateTo({
      url: '../technicain_detail/technicain_detail?tId=' + tId,
    })
  }
})