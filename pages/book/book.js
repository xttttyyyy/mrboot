//require获取本机其他js文件并保存到变量
var util = require("../../utils/util.js")
var data = require("../../utils/data.js")
// pages/book/book.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 当前项目的id
      suId:null,
    
    //当前用户的别名
      nickname:'X',
    // 商家信息
      sname: "",
    // 项目信息
      suname: "",
      date:'2017-09-01',
      time:'01:01',
    //留言
      val:''



  },
  onLoad: function (options) {
    var that = this
    this.setData({
      suId:options.suId
    })
    // 获取用户名
    wx.getUserInfo({
      success: (result) => {
        console.log(result.userInfo.nickName)
        this.setData({
          nickname:result.userInfo.nickName
        })
        //初始化地址信息
        wx.request({
          url: 'http://localhost:8080/order/queryNameById',
          data:{
            suId: options.suId
          },
          success:function(res){
            console.log(res)
            that.setData({
              sname: res.data.data.sname,
              suname: res.data.data.suname
            })
          }
        })
      } 
      
    })

  },
  // // 地址选择
  // bindAddrPickerChange:function(event){
    

  //   var vall = event.detail.value;
  //   // console.log(vall)
  //   // console.log(this.data.addrs[vall])

  //   // 重新设置 addrIndex属性，同步更新页面地址数据
  //   //把当前的地址id更改
  //   this.setData({
  //     addrIndex: event.detail.value,
  //     addrId:this.data.addrs[vall].aid
  //   })
  // },


  // 日期选择
  bindDateChange: function (event) {
    console.log("picker标签的value属性" + event.detail.value);
    // 重新设置 date属性，同步更新页面地址数据
    this.setData({
      date: event.detail.value
    })
  },
  // 时间选择
  bindTimeChange: function (event) {
    console.log("picker标签的value属性" + event.detail.value);
    // 重新设置 time属性，同步更新页面地址数据
    this.setData({
      time: event.detail.value
    })
  },

  changeMessage:function(event){
    console.log(event)
    this.setData({
      val:event.detail.value
    })

  },

  // 向后台发送请求 实现数据添加
  bindToastTap:function(){
    wx.request({
      url: 'http://localhost:8080/order/insertOrder',
      method:'POST',
      data:
      {
        message:this.data.val,
        dur:this.data.date+' '+this.data.time,
        nickname:this.data.nickname,
        suId:this.data.suId,
        sname:this.data.sname,
        suname: this.data.suname,
        value: this.data.val
      },
      success:function(res){
        console.log(res)
        // 预约成功后显示弹框
        wx.showToast({
          title: '预约成功',
          icon: 'success',
          duration: 2000
        })
        // 注意

      }
    })

    
  },
  // 隐藏提示框
  hideToast: function () {
    this.setData({
      bookToastHidden: true
    })
  }
})