
Page({

  // 当前页面的数据
  data: {
    // banner 初始化数据  key-value
    'banner': [],
    // 导航栏的数据
    navTopItems: [],
    // 列表内容的下标静态的时候使用
    curIndex:0,
    // 导航栏列表内容数据
    navSection: [],
    colors:["red","orange","yellow","green","purple"],
    indicatorDots: true,
    vertical:false,
    autoplay:true,
    interval:3000,
    duration:1000,
    // 选中id
    curNavId:1 
  },


  // 加载页面监听 ---当加载当前页面的时候，执行的函数
  onLoad: function () {
    var that = this;
    // 获取用户信息,得到用户昵称
    wx.getUserInfo({
      success: function(res) {
        var nicname = res.userInfo.nickName
        
      }
    })
    //向后台服务器发送请求获取轮播图数据
    wx.request({
      url: 'http://localhost:8080/pic/queryByType/lb',
      success:function(res){
        // console.log(res)
        that.setData({
          banner:res.data.data
        })
      }
    })

    //向服务器获取导航栏数据
    wx.request({
      url: 'http://localhost:8080/pic/queryByType/nav',
      success:function(res){
        // console.log(res)
        that.setData({
        navTopItems:res.data.data
        })
      }
    }),
    //向服务器获取默认推荐数据z
    wx.request({
      url: 'http://localhost:8080/subject/queryByTitle/推荐',
      success:function(res){
        console.log(res)
        that.setData({
        navSection:res.data.data
        })
      }
    })
  },

  

  // 实现点击导航栏按钮以后加载对应的数据
  getSubject:function(event){
    var that = this;
    console.log(event)
    //向服务器发送请求   ---- title（项目名称）  res.data.data
    var title = event.currentTarget.dataset.title
    wx.request({
      url: 'http://localhost:8080/subject/queryByTitle/'+title,
      success:function(res){
        ////将数据封装到data的navSelection
        that.setData({
          navSection:res.data.data
        })
      }
    })
  },





  

//参数有问题
  getPic:function(type,dataname){
    wx.request({
      url: 'http://localhost:8080/pic/queryByType/'+type,
      success:function(res){
        console.log(res)
        that.setData({
           "'+ dataname+'":res.data.data
        })
      }
    })


    this.setData({
      list:this.data.navSection
    })
  },


//跳转到详情页
navgateDetail:function(event){
  console.log(event)
  //页面跳转   ---页面跳转以后 需要展示当前subject的详细信息
  var id = event.currentTarget.dataset.id
  //进行页面跳转
  wx.navigateTo({
    url: '../detail/detail?id='+id,
  })
},

  bookTap: function (item) {
    console.log(item)
    var id = item.currentTarget.dataset.id
    wx.navigateTo({
      url: '../book/book?suId='+id,
    })
  }
})
