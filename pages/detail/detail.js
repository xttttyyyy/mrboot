// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    subject:null
  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.id)
    var that = this
    wx.request({
      url: 'http://localhost:8080/subject/queryById/'+options.id,
      success:function(res){
        console.log(res)
        that.setData({
          subject:res.data.data
        })

      }
    })

  },
  // 跳转预定页面
  bookTap: function (event) {
    console.log(event)
    wx.navigateTo({
      url: '../book/book?suId='+ event.currentTarget.dataset.id,
    })
  },

  
})