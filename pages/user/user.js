//index.js
//获取应用实例
var app = getApp()
var myData = require('../../utils/data')
var util = require('../../utils/util')
// var getReservationService = require('../../utils/data')

Page({
  // 页面初始数据
  data: {
    userData:myData.userData(),
    // addrDate:util.replacePhone(myData.userData().addrs,true),
  },
  onLoad: function(){
    var that = this
    wx.getUserInfo({
      success: (result) => {
        console.log(result.userInfo.nickName)
        this.setData({
          nickname:result.userInfo.nickName
        })
        //初始化地址信息
        wx.request({
          url: 'http://localhost:8080/order/queryByWX',
          data:{
            wxname: this.data.nickname
          },
          success:function(res){
            console.log(res)
            that.setData({
              reservationService: res.data
            })
          }
        })
      } 
    })
  },
  // 地址编辑
  // editAddr : function(e){
  //   console.log(e)
  //   wx.navigateTo({
  //     url:'../edit_addr/edit_addr?addrid='+e.currentTarget.dataset.addrid
  //   })
  // }

})
