// pages/technicain_detail/technicain_detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 商家服务内容
    subjectByShop: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(options)
    this.setData({
      tId : options.tId
    })
    // 查找当前技师在商家可选技能/techn/querySubjectByShop
    wx.request({
      url: 'http://localhost:8080/techn/querySubjectByShop',
      data:{
        tId: that.data.tId
      },
      success:function(res){
        console.log(res)
        that.setData({
          subjectByShop: res.data
        })
      }
    })
  },
  bookTap:function(event){
    wx.navigateTo({
      url:'../book/book?suId='+ event.currentTarget.dataset.id
    })
  }
})